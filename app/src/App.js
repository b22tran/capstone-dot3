import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Login from "./components/Login";
import Search from "./components/Search";
import About from "./components/About";
import Error from "./components/Error";
import Navigation from "./components/Navigation";



class App extends React.Component {

  
  componentDidMount(){
     }

  //render
  render() {
    return (

      <BrowserRouter>
        <div>
       
          {/* include outside so it shows up on every page`
              change later so it doesnt show on login page*/}
          <Navigation/>
          {/* using Switch so it doesnt show error each time */}
          <Switch>
            <Route path="/" component = {Search} exact />
            <Route path="/login" component = {Login} />
            <Route path="/about" component = {About} />
            <Route component = {Error} />
        </Switch>
        </div>
      </BrowserRouter>
     
    );
  }

};

export default App;