import React from "react";
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';

const StudentSearch = props => (
    <div>
    <form onSubmit={props.getStudent}>
    <Input placeholder="Eg. 10000000" type="text" />
    <Button type="submit" variant="contained" color="primary">Get Student Info</Button>
    </form>
    <div><br></br></div>
    <form onSubmit={props.getStudents}>
        <Button type="submit" variant="contained" color="primary">Get All Students</Button>
    </form>
</div>

);

export default StudentSearch;