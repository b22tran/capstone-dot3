import React from "react";
import { NavLink } from "react-router-dom";

const SearchNavigation = () => {
    return(
        <div>
            <NavLink to="/course">Course Search</NavLink>
            <NavLink to="/student">Student Search</NavLink>
        </div>
    );

}

export default SearchNavigation;