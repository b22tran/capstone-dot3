import React from "react";

// function _renderObject() {
//     return (
//         <div>
//             id is: {ObjectTest[obj].id} ;
//                 name is: {ObjectTest[obj].name}
//         </div>
//     )
// }

const Courses = props => (
    <div className="course__info">

        {
            props.cid && <p className="course__key">Course ID:
            <span className="course__value"> {props.cid}</span>
            </p>
        }
        {
            props.cname && <p className="course__key">Name:
            <span className="course__value"> {props.cname}</span>
            </p>
        }

        {
            props.cdesc && <p className="course__key">Description:
            <span className="course__value"> {props.cdesc}</span>
            </p>
        }

        {
            props.ccredits && <p className="course__key">Weight:
            <span className="course__value"> {props.ccredits}</span>
            </p>
        }
        {
            props.cinstitution && <p className="course__key">Institution:
            <span className="course__value"> {props.cinstitution}</span>
            </p>
        }
        {
            props.error && <p className="course__error">{props.error}</p>
        }


    </div>
)



export default Courses;