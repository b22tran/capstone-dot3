import React from "react";
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';


const CourseSearch = props => (
    <div>

        
        <form onSubmit={props.getCourse}>
        <Input placeholder="Eg. HU0001" type="text" name = "cid"/>
        <Button type="submit" variant="contained" color="primary">Get Course Info</Button>
        </form> 
        <div><br></br></div>
        <form onSubmit={props.getCourses}>
        <Button type="submit" variant="contained" color="primary">Get All Courses</Button>
        </form>

        
    </div>
    
);

export default CourseSearch;