import React from "react";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
//import CourseSearch from "./CourseSearch";
//import StudentSearch from "./StudentSearch";
import Courses from "./Courses";
import Students from "./Students";
import Input from '@material-ui/core/Input';
import Button from "@material-ui/core/Button";

class Search extends React.Component {

  state = {

    cid: undefined,
    cname: undefined,
    cdesc: undefined,
    ccredits: undefined,
    cinstitution: undefined,

    sfname: undefined,
    smname: undefined,
    slname: undefined,

    error: undefined,
    listQuery: [],

    cRows: []


  }


  //getCourse func
  getCourse = async (e) => {
    //prevent full page load
    e.preventDefault();

    const cid = e.target.elements.cid.value;

    //using async await
    const api_call = await fetch("http://localhost:8081/course/"
    + cid);
    const data = await api_call.json();
    var rows = [];
      let row = data.id + "\t" + data.name + "\t" + data.desc + "\t" + data.credits + "\t" + data.institution; 
      rows.push(row)
    this.setState({ cRows: rows })

    // if (cid) {
    //   console.log("cid exists : " + cid);
    //   //set state
    //   this.setState({
    //     cid: data.id,
    //     cname: data.name,
    //     cdesc: data.desc,
    //     ccredits: data.credits,
    //     cinstitution: data.institution,
    //     error: ""
    //   });

    // } else {
    //   console.log("no cid");
    //   this.setState({
    //     cid: undefined,
    //     cname: undefined,
    //     cdesc: undefined,
    //     ccredits: undefined,
    //     cinstitution: undefined,
    //     error: "Unknown course id"
    //   });
    // }
  }

  //getCourses func
  getCourses = async (e) =>{
      console.log("All Courses called")
    e.preventDefault();
    const api_call = await fetch('http://localhost:8081/courses');
    const data = await api_call.json();
    console.log(data);
    var rows = [];
    data.forEach(element => {
      let row = "\t" + element.id + "\t" + element.name + "\t" + element.desc + "\t" + element.credits; 
      rows.push(row)
    })
    this.setState({ cRows: rows })
  }

  //getCourse func
  getUsers = async (e) => {
    //prevent full page load
    e.preventDefault();

    //using async await
    const api_call = await fetch("https://jsonplaceholder.typicode.com/users");
    const data = await api_call.json();
    console.log(data);
    var rows = [];
    data.forEach(element => {
      let row = "\t" + element.id + "\t" + element.name + "\t" + element.username + "\t" + element.email + "\t" + element.phone; 
      rows.push(row)
    })
    this.setState({ cRows: rows })
  }

  //getStudents func
  getStudent = async (e) => {
    //prevent full page load
    e.preventDefault();

    const sid = e.target.elements.sid.value;

    //using async await
    const api_call = await fetch("http://localhost:8081/student/"+ sid);
    const data = await api_call.json();
    var rows = [];
      let row = data.id + "\t" + data.firstName + "\t" + data.middleName + "\t" + data.lastName; 
      rows.push(row)
    this.setState({ cRows: rows })
    // if (sid) {
    //   console.log("sid exists : " + sid);
    //   //set state
    //   this.setState({

    //     sfname: data.firstName,
    //     smname: data.middleName,
    //     slname: data.lastName,
    //     error: ""
    //   });

    // } else {
    //   this.setState({
    //     sfname: undefined,
    //     smname: undefined,
    //     slname: undefined,
    //     error: "No student.."
    //   });
    // }
  }


    //getStudents func
    getStudents = async (e) =>{
      console.log("All Students called")
      e.preventDefault();
      const api_call = await fetch('http://localhost:8081/students');
      const data = await api_call.json();
      console.log(data);
      var rows = [];
      data.forEach(element => {
        let row =  element.id +"\t" + element.firstName + "\t" + element.middleName + "\t" + element.lastName; 
        rows.push(row)
      })
      this.setState({ cRows: rows })
      }
     
      // for (i = 0; i< data.length(); i++){
      //   console.log(data[i]);
      // };       

    render(){
    return(
        <div>
            
            <Grid container spacing={24}>
            <Grid item xs={12}>
            <h3>Hello AcaLink User! Query Below</h3>
          </Grid>

          <Grid item xs={12}>
            <Paper >
              <h5 >Search Course by ID</h5>
            </Paper></Grid>

          <Grid item xs={12}>
            <Paper>
              <form onSubmit={this.getCourse}>
                <Button type="submit" variant="contained" color="primary">Get Course Info</Button>
                <Input placeholder="Eg. HU0001" type="text" required="true" name="cid" />
              </form>
              <br></br>
                <Button onClick={this.getCourses} type="submit" variant="contained" color="primary">Get All Courses</Button>
              {/* <CourseSearch/> */}
              </Paper>
            </Grid>

          <Grid item xs={12}>
            <Paper>
              <h5>View All Students</h5>
            </Paper></Grid>

          <Grid item xs={12}>
            <Paper>
              <form onSubmit={this.getStudent}>
                <Button type="submit" variant="contained" color="primary">Get Student Info</Button>
                <Input placeholder="Eg. 10000000" type="text" required="true" name="sid" />
              </form>
             <br></br>
                <Button onClick={this.getStudents} type="submit" variant="contained" color="primary">Get All Students</Button>
              {/* <StudentSearch /> */}
            </Paper></Grid>
        </Grid>


        <Grid item m={12}>
          <Paper>
            <h5>Data Output:</h5>
            <div id="dataoutput">

              <table>
                {this.state.cRows.map((r) => (
                  <tr>
                    {r}
                  </tr>
                ))}
              </table>
              <Students
                sfname={this.state.sfname}
                smname={this.state.smname}
                slname={this.state.slname}
              />

            </div></Paper></Grid>
      </div >
    );
  }

};

export default Search;