import React from "react";

const Students = props => (
    <div className="student__info">

        {
            props.sfname && <p className="student__key">First Name:
            <span className="student__value"> {props.sfname}</span>
            </p>
        }

        {
            props.smname && <p className="student__key">Middle Name:
            <span className="student__value"> {props.smname}</span>
            </p>
        }

        {
            props.slname && <p className="student__key">Last Name:
            <span className="student__value"> {props.slname}</span>
            </p>
        }
        {
            props.error && <p className="student__error">{props.error}</p>
        }
    </div>
)
export default Students;