import React from "react";
import Button from "@material-ui/core/Button";
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';


const Login = () => {
    return(
        <form >
        <InputLabel htmlFor="username">Login</InputLabel>
        <Input id="username" required="true"

         /> 
        <InputLabel htmlFor="password">Password</InputLabel>
        <Input id="password" type="password" required="true"
         />
         <br></br><br></br>
          <Button type="submit" variant="contained" color="primary"><span>Login</span></Button>
          <div class="footer"><a href="">Forgot your password?</a></div>
        </form>
    );

}

export default Login;

