import React from "react";
import SvgIcon from '@material-ui/core/SvgIcon';

function HomeIcon(props) {
    return (
        // unpacked props
      <SvgIcon {...props}>
        <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
      </SvgIcon>
    );
  }
  function AboutIcon(props) {
    return (
        // unpacked props
      <SvgIcon {...props}>
        <path  d="M9 1C4.58 1 1 4.58 1 9s3.58 8 8 8 8-3.58 8-8-3.58-8-8-8zm0 2.75c1.24 0 2.25 1.01 2.25 2.25S10.24 8.25 9 8.25 6.75 7.24 6.75 6 7.76 3.75 9 3.75zM9 14.5c-1.86 0-3.49-.92-4.49-2.33C4.62 10.72 7.53 10 9 10c1.47 0 4.38.72 4.49 2.17-1 1.41-2.63 2.33-4.49 2.33z"/>
      </SvgIcon>
    );
  }

  function SearchIcon(props) {
    return (
        // unpacked props
      <SvgIcon {...props}>
        <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
      </SvgIcon>
    );
  }

const Navigation = () => {
    return(

    <nav>
        <div >
            <h1>AcaLink</h1>
            <a href='/login'> <HomeIcon/></a>
                <a href='/'><SearchIcon/></a>
                <a href='/about'><AboutIcon/></a>
            <h1><br></br></h1>
        </div>
    </nav>
    );

}

export default Navigation;