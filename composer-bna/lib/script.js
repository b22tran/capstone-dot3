/* global getAssetRegistry getFactory */

/**
 * Publish a new transcript
 * @param {org.acme.acalink.enrollCourse} course - transcation for enrolling course
 * @transaction
 */
async function enrollCourse(course) {
    console.log('enrollCourse');

    const factory = getFactory();

    // Create the transcript asset.
    const courseAsset = factory.newResource('org.acme.acalink', 'EnrolledCourse', course.enrollId);
    courseAsset.course = course.course;
    courseAsset.student = course.student;
    courseAsset.courseGrade = course.courseGrade;
    courseAsset.status = course.status;
    courseAsset.completionDate = course.completionDate;

    // Save the enroll course
    const registry = await getAssetRegistry('org.acme.acalink.EnrolledCourse');
    // Add the enrolled course asset to the registry.
    await registry.add(courseAsset);

   // emit the event
    const enrollCourseEvent = factory.newEvent('org.acme.acalink', 'eventEnrollCourse');
    enrollCourseEvent.enrollId = course.enrollId;
    enrollCourseEvent.course = course.course;
    enrollCourseEvent.student = course.student;
    enrollCourseEvent.courseGrade = course.courseGrade;
    enrollCourseEvent.status = course.status;
    enrollCourseEvent.completionDate = course.completionDate;
    emit(enrollCourseEvent);
}


/**
* Publish a new transcript
* @param {org.acme.acalink.publishTranscript} transcript - transcation for publishing a transcript
* @transaction
*/
async function publishTranscript(transcript) {
    console.log('publishTranscript');

    const factory = getFactory();

    // Create the transcript asset.
    const transcriptAsset = factory.newResource('org.acme.acalink', 'Transcript', transcript.id);
    transcriptAsset.student = transcript.student;
    transcriptAsset.institution = transcript.institution;
    transcriptAsset.enrolledCourses = transcript.enrolledCourses;
    transcriptAsset.completionDate = transcript.completionDate;

    // Save the transcript
    const registry = await getAssetRegistry('org.acme.acalink.Transcript');
    // Add the transcript asset to the registry.
    await registry.add(transcriptAsset);


    // emit the event
    const transcriptEvent = factory.newEvent('org.acme.acalink', 'eventPublishTranscript'); 
    transcriptEvent.id = transcript.id;
    transcriptEvent.student = transcript.student;
    transcriptEvent.institution = transcript.institution;
    transcriptEvent.enrolledCourses = transcript.enrolledCourses;
    transcriptEvent.completionDate = transcript.completionDate;
    emit(transcriptEvent);

}


// Script for demo creation
/**
* Create the participants and assets to use in the demo
* @param {org.acme.acalink.SetupDemo} setupDemo - the SetupDemo transaction
* @transaction
*/
async function setupDemo() { 
  console.log('setupDemo');

  const factory = getFactory();
  const namespace = 'org.acme.acalink';


// create Students

  student = factory.newResource(namespace, 'Student', '10000000');
  student.firstName = 'Paul';
  student.middleName = 'James';
  student.lastName = 'Smith';
  studentRegistry = await getParticipantRegistry(namespace + '.Student');
  await studentRegistry.add(student);


  student = factory.newResource(namespace, 'Student', '20000000');
  student.firstName = 'Andy';
  student.lastName = 'Chan';
  studentRegistry = await getParticipantRegistry(namespace + '.Student');
  await studentRegistry.add(student);

  student = factory.newResource(namespace, 'Student', '30000000');
  student.firstName = 'Hannah';
  student.lastName = 'White';
  studentRegistry = await getParticipantRegistry(namespace + '.Student');
  await studentRegistry.add(student);

  student = factory.newResource(namespace, 'Student', '40000000');
  student.firstName = 'Sam';
  student.middleName = 'Jimmy';
  student.lastName = 'Hettler';
  studentRegistry = await getParticipantRegistry(namespace + '.Student');
  await studentRegistry.add(student);

  student = factory.newResource(namespace, 'Student', '50000000');
  student.firstName = 'Caroline';
  student.lastName = 'Chevy';
  studentRegistry = await getParticipantRegistry(namespace + '.Student');
  await studentRegistry.add(student);

  student = factory.newResource(namespace, 'Student', '60000000');
  student.firstName = 'James';
  student.middleName = 'Xiao';
  student.lastName = 'Le';
  studentRegistry = await getParticipantRegistry(namespace + '.Student');
  await studentRegistry.add(student);

  student = factory.newResource(namespace, 'Student', '70000000');
  student.firstName = 'Dave';
  student.lastName = 'Roberts';
  studentRegistry = await getParticipantRegistry(namespace + '.Student');
  await studentRegistry.add(student);

  student = factory.newResource(namespace, 'Student', '80000000');
  student.firstName = 'Ellie';
  student.lastName = 'Ann';
  studentRegistry = await getParticipantRegistry(namespace + '.Student');
  await studentRegistry.add(student);

  student = factory.newResource(namespace, 'Student', '90000000');
  student.firstName = 'Mark';
  student.lastName = 'Weins';
  studentRegistry = await getParticipantRegistry(namespace + '.Student');
  await studentRegistry.add(student);

  // create Institutions 
  institution = factory.newResource(namespace, 'Institution', '10001000');
  institution.name = 'Sheridan';
  institution.primaryType = 'COLLEGE';
  institutionRegistry = await getParticipantRegistry(namespace + '.Institution');
  await institutionRegistry.add(institution);

  institution = factory.newResource(namespace, 'Institution', '20002000');
  institution.name = 'Humber';
  institution.primaryType = 'COLLEGE';
  institutionRegistry = await getParticipantRegistry(namespace + '.Institution');
  await institutionRegistry.add(institution);

  institution = factory.newResource(namespace, 'Institution', '30003000');
  institution.name = 'Waterloo';
  institution.primaryType = 'UNIVERSITY';
  institutionRegistry = await getParticipantRegistry(namespace + '.Institution');
  await institutionRegistry.add(institution);

  institution = factory.newResource(namespace, 'Institution', '40004000');
  institution.name = 'Ryerson';
  institution.primaryType = 'UNIVERSITY';
  institutionRegistry = await getParticipantRegistry(namespace + '.Institution');
  await institutionRegistry.add(institution);

  institution = factory.newResource(namespace, 'Institution', '50005000');
  institution.name = 'Udemy';
  institution.primaryType = 'CERTIFICATE';
  institutionRegistry = await getParticipantRegistry(namespace + '.Institution');
  await institutionRegistry.add(institution);


   // create Courses 
  course = factory.newResource(namespace, 'Course', 'SH0001');
  ins = factory.newResource(namespace, 'Institution', '10001000')
  course.name = 'PROG1000';
  course.desc = 'Intro to Java Programming';
  course.credits = '6.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);
  
  course = factory.newResource(namespace, 'Course', 'SH0002');
  ins = factory.newResource(namespace, 'Institution', '10001000')
  course.name = 'PROG1001';
  course.desc = 'Programming Logic';
  course.credits = '4.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'SH0003');
  ins = factory.newResource(namespace, 'Institution', '10001000')
  course.name = 'MATH1000';
  course.desc = 'Applied Calculus';
  course.credits = '4.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'SH0004');
  ins = factory.newResource(namespace, 'Institution', '10001000')
  course.name = 'DATA1000';
  course.desc = 'Data Science 101';
  course.credits = '4.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'SH0005');
  ins = factory.newResource(namespace, 'Institution', '10001000')
  course.name = 'INFO1000';
  course.desc = 'CST Capstone Research';
  course.credits = '4.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'HU0001');
  ins = factory.newResource(namespace, 'Institution', '20002000')
  course.name = 'PROG2000';
  course.desc = 'Game Engineering';
  course.credits = '4.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'HU0002');
  ins = factory.newResource(namespace, 'Institution', '20002000')
  course.name = 'PROG1000';
  course.desc = 'Programming 101';
  course.credits = '6.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'WA0001');
  ins = factory.newResource(namespace, 'Institution', '30003000')
  course.name = 'BIO1000';
  course.desc = 'Virology';
  course.credits = '4.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'WA0002');
  ins = factory.newResource(namespace, 'Institution', '30003000')
  course.name = 'PROG1000';
  course.desc = 'Intro to Data Science';
  course.credits = '4.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'RY0001');
  ins = factory.newResource(namespace, 'Institution', '40004000')
  course.name = 'PROG1000';
  course.desc = 'Intro to Programming';
  course.credits = '6.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'RY0002');
  ins = factory.newResource(namespace, 'Institution', '40004000')
  course.name = 'MATH1000';
  course.desc = 'Discrete Math and Applied Calculus';
  course.credits = '6.0';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'UD0002');
  ins = factory.newResource(namespace, 'Institution', '50005000')
  course.name = 'MATH1234';
  course.desc = 'Statistics for Business Analytics';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

  course = factory.newResource(namespace, 'Course', 'UD0002');
  ins = factory.newResource(namespace, 'Institution', '50005000')
  course.name = 'PROG6000';
  course.desc = 'Machine Learning in Python and R';
  course.institution = ins;
  courseRegistry = await getAssetRegistry(namespace + '.Course');
  await courseRegistry.add(course);

}