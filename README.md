# AcaLink test versions
This is a test version for AcaLink.

The commands below are setup with this folder structure, all commands are run from fabric-dev-servers
./  |-fabric-dev-servers/
    |-capstone-dot3/

# Prerequisites for Hyperledger Fabric & Composer
Ensure that you've created a local instance of your development environment from the following link
>If you are using Ubuntu 18.04 or any other current unsupported versions, edit your prereqs-ubuntu.sh file to include 'bionic' in the versions.
You may also need to force install the packages themselves.
> **https://hyperledger.github.io/composer/latest/installing/installing-index**

# Business Network Definition
This business network defines:

**Enum**

`CourseType`
`InstitutionType`

**Participant**

`Student`
`Institution`

**Asset**

`Course`
`EnrolledCourse`
`Transcript`

**Transaction**

`publishTranscript`
`retrieveTranscript`

**Event**

`eventPublishTransactions`

# Starting, Stopping and Tearing down your hlf
>**https://hyperledger.github.io/composer/latest/installing/development-tools**

# Creating a business network archive (.bna)
*OPTIONAL - Only needs to be run if there is no bna*
.bna file can be created from your current directory into dist directory by utilizing the command 
> composer archive create -t dir -n . -a ../capstone-dot3/composer-bna/dist/acalink.bna

# Linking PeerAdmin(ChannelAdmin) to the business network (acalink) (in ./fabric-tools folder)
** Change the deploy version to the must updated one, based on the file name **

Deploy bna file to the HLF
> composer network install --archiveFile ../capstone-dot3/composer-bna/dist/acalink.bna --card PeerAdmin@hlfv1

Create an admin card in relation to acalink.bna
> composer network start -n acalink -V "0.0.2-deploy.12" -c PeerAdmin@hlfv1 -A admin -S adminpw

*OPTIONAL - If the previous two commands doesn't work then try..*
> *composer network start -n acalink -V "0.0.2-deploy.12" -c PeerAdmin@hlfv1 -A admin -S adminpw -f ../capstone-dot3/composer-bna/dist/acalink.bna*

Import the card into composer
> composer card import -f ./admin@acalink.card

Verify if your card has been successfully imported into the store and ping it
> composer card list
> composer network ping -c admin@acalink

Set-up composer REST server in a sinle-user mode
> composer-rest-server
>
> ? Enter the name of the business network card to use: admin@acalink
> 
> ? Specify if you want namespaces in the generated REST API: always use namespaces 
>
> ? Specify if you want to enable authentication for the REST API using Passport: No 
> 
> ? Specify if you want to enable event publication over WebSockets: No 
>
> ? Specify if you want to enable TLS security for the REST API: No 
>
> *To restart the REST server using the same options, issue the following command: \
   composer-rest-server -c admin@acalink -n always*

Access the RESTful API swagger below:
> http://localhost:3000/explorer

**If you've deployed a .bna but it's outdated, use composer network upgrade**
